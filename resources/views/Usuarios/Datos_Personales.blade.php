@extends('layouts.app')
@section('content')


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
$( document ).ready(function() {


	function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp").change(function() {
  readURL(this);
});



@if($mensage==null)
 $("#alert").hide();
 @else
 $("#alert").fadeIn( 2000 );
 $("#alert").slideUp( 4000 );
 @endif
});
</script>




<div class="container">
   <div class="alert alert-success" id="alert" style="display:none;" >
  <strong>{{$mensage}}</strong> 
</div>
 
  <div class="row" style="border:1px solid gray;border-radius: 5px;
 box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
   	



<div  class=" col-sm-12 col-md-12 col-lg-12 col-xl-12" style="background: rgba(0, 0, 0, 0.9);">
   		 <h3 class="text-center" style="color:white;">DATOS PERSONALES</h3>
</div>

<div  class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style="background: rgba(255, 255, 255, 0.5);">

<form method="POST" action="/Datos_p/{{$datos->id}}" enctype="multipart/form-data" >
       {{ csrf_field()}}

<div class="row ">
	<div class="col-md-12"style="margin:1em;"></div>
	<div class="col-md-3" >
		<div class="card" >
  			
            <img class="card-img-top" id="blah" src="/Avatar/{{$datos->imagen}}"
            style="height:16em;">
  			<div class="card-body">
    			
  
  			<div class="row">
 				<div class="Uploadbtn" style="
  position: relative;
  overflow: hidden;
  padding:5px 5px;
  text-transform: uppercase;
  color:#fff;
  background: #000066;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  -ms-border-radius: 4px;
  -o-border-radius: 4px;
   border-radius: 4px;
  width:50px;
  text-align:center;
  cursor: pointer;">
  <input type="file" id="imgInp" name="imagen" class="form-group " accept="image/x-png,image/gif,image/jpeg" style="
  position: absolute;
  top: 0;
  right: 0;
  margin: 0;
  padding: 0;
  opacity: 0;
  height:50%;
  width:50%;"/>
  <span><i class="material-icons">
add_a_photo
</i></span>
</div>
<button type="submit" class="btn btn-success">Guardar</button>

</div>
    		
    			
  			</div>
		</div>

	</div>


	<div class="row col-md-9" >


		<div class="container">

		<nav>
  			<div class="nav nav-tabs" id="nav-tab" role="tablist">
    		<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Datos Personales</a>
    		<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Direccion</a>
  			</div>
		</nav>
		<div class="tab-content" id="nav-tabContent">
 		 	<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
  			
<div class="row">
<div class="col-md-12"style="margin:1em;"></div>
		 <div class="form-group col-md-4">
    		<label for="formGroupExampleInput">NOMBRE:</label>
    		<input type="text" class="form-control" id="formGroupExampleInput" required 
    		 style="text-transform:uppercase;" name="nombre" value="{{$datos->nombre}}">
  		 </div>

		<div class="form-group col-md-4">
    		<label for="formGroupExampleInput">APELLIDO PATERNO:</label>
    		<input type="text" class="form-control" id="formGroupExampleInput" required 
    		 style="text-transform:uppercase;" name="ap" value="{{$datos->ap}}">
  		</div>

  		<div class="form-group col-md-4">
    		<label for="formGroupExampleInput">APELLIDO MATERNO:</label>
    		<input type="text" class="form-control" id="formGroupExampleInput" required
    		 style="text-transform:uppercase;" name="am" value="{{$datos->am}}">
  		</div>

 

  		 <div class="form-group col-md-2">
    		<label for="exampleFormControlSelect1">GENERO:</label>
    		<select class="form-control" id="exampleFormControlSelect1" required
    		 style="text-transform:uppercase;" name="genero" value="{{$datos->genero}}">
         @if($datos->genero=='')
      		<option value="1">F</option>
     		  <option value="2">M</option>
         @endif
         @if($datos->genero==1)
          <option value="1" selected="selected">F</option>
          <option value="2">M</option>
         @endif
         @if($datos->genero==2)
          <option value="1">F</option>
          <option value="2"  selected="selected">M</option>
         @endif
    		</select>
  		</div>

  		<div class="form-group col-md-2">
    		<label for="formGroupExampleInput">EDAD:</label>
    		<input type="text" class="form-control" id="formGroupExampleInput" required name="edad" value="{{$datos->edad}}">
  		</div>


  		 <div class="form-group col-md-4">
   			 <label for="formGroupExampleInput">No. TELEFONO:</label>
    		<input type="text" class="form-control" id="formGroupExampleInput" name="telefono" value="{{$datos->telefono}}">
  		</div>

		<div class="form-group col-md-4">
    		<label for="formGroupExampleInput">No. CELULAR:</label>
    		<input type="text" class="form-control" id="formGroupExampleInput" name="celular" value="{{$datos->celular}}">
  		</div>
  	<div class="form-group col-md-4">
        <label for="exampleFormControlSelect1">NACIONALIDAD:</label>
        <select class="form-control" id="exampleFormControlSelect1" required
         style="text-transform:uppercase;" name="genero" value="{{$datos->nacionalidad}}">
         @if($datos->nacionalidad=='')
          <option value="1">MEXICANA</option>
          <option value="2">EXTRANJERA</option>
         @endif
         @if($datos->nacionalidad==1)
          <option value="1" selected="selected">MEXICANA</option>
          <option value="2">EXTRANJERA</option>
         @endif
         @if($datos->nacionalidad==2)
          <option value="1">MEXICANA</option>
          <option value="2"  selected="selected">EXTRANJERA</option>
         @endif
        </select>
      </div>
  		<div class="form-group col-md-4" style="display:none;">
    		<label for="formGroupExampleInput">id:</label>
    		<input type="text" class="form-control" id="formGroupExampleInput" name="id" value="{{$datos->id}}">
  		</div>


</div>
  			</div>

  			<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
  			
				<div class="row">

						
				<div class="col-md-12"style="margin:1em;"></div>

  						<div class="form-group col-md-2">
    					<label for="exampleFormControlSelect1">MUNICIPIO:</label>
    					<select class="form-control" id="exampleFormControlSelect1" required
    		 			style="text-transform:uppercase;" name="municipio" value="{{$datos->id_mun_dir}}">
      					<option value="1">Valle de Bravo</option>
    					</select>
  						</div>

  						<div class="form-group col-md-4">
    					<label for="formGroupExampleInput">LOCALIDAD:</label>
    					<input type="text" class="form-control" id="formGroupExampleInput" 
    					 required
    		 			 style="text-transform:uppercase;" name="localidad" value="{{$datos->localidad}}">
  						</div>

  						<div class="form-group col-md-4">
    					<label for="formGroupExampleInput">COLONIA:</label>
    					<input type="text" class="form-control" id="formGroupExampleInput" 
    					 required
    					 style="text-transform:uppercase;" name="colonia" value="{{$datos->colonia}}">
  						</div>

  						<div class="form-group col-md-4">
    					<label for="formGroupExampleInput">CALLE:</label>
    					<input type="text" class="form-control" id="formGroupExampleInput"
    					 
    		 			 style="text-transform:uppercase;" name="calle" value="{{$datos->calle}}">
  						</div>

  						<div class="form-group col-md-2">
    					<label for="formGroupExampleInput">NO INTERIOR:</label>
    					<input type="text" class="form-control" id="formGroupExampleInput" name="noi" value="{{$datos->no_i}}">
  						</div>
  						<div class="form-group col-md-2">
    					<label for="formGroupExampleInput">NO EXTERIOR:</label>
    					<input type="text" class="form-control" id="formGroupExampleInput" name="noe" value="{{$datos->no_e}}">
  						</div>

  						<div class="form-group col-md-4">
    					<label for="formGroupExampleInput">OTRA REFERENCIA:</label>
    					<input type="text" class="form-control" id="formGroupExampleInput" 
    					
    		             style="text-transform:uppercase;" name="otra_ref" value="{{$datos->otra_ref}}">
  						</div>


				</div>


</form>


  			</div>
		</div>

		</div>	
	</div>

			

	<div class="col-md-12"style="margin:1em;"></div>



</div>
	</div>


</div>

</div>





</div>




    
  </div>
</div>

@endsection