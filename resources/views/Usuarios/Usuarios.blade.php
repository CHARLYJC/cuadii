@extends('layouts.app')

@section('content')

 

<div class="container">
  <div class="row">
   	



<div  class=" col-sm-12 col-md-12 col-lg-12 col-xl-12" style="background: rgba(0, 0, 0, 0.9);">
   		 <h3 class="text-center" style="color:white;">USUARIOS</h3>
</div>

<div  class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style="background: rgba(0, 0, 0, 0.5);">
</br></br>


	<div class="accordion" id="accordionExample">
		@foreach($usuarios as $u)
  		<div class="card">
   			 <div class="card-header" id="headingOne">
    			  <h5 class="mb-0">
	
       				 <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{$u["Nombre"]}}" aria-expanded="true" aria-controls="collapseOne">
       				   {{$u["Nombre"]}}
        			</button>
      			   </h5>
    		</div>

    		<div id="collapse{{$u["Nombre"]}}" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
      			<div class="card-body row">
					@foreach($u["us"] as $us)
					<div class="col-md-3">
						<div class="card "style="border:1px solid gray;border-radius: 5px;
 box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);" >
 						 <img class="card-img-top" src="img/usr.png" alt="Card image cap">
  						 	<div class="card-body " >
   								<h5 class="card-title">{{$us['name']}}</h5>
   								<div class="row">
   								<a href="#" class="btn btn-primary">Ver datos</a>
   								<form class="form" role="form" method="POST" id="{{$us["id"]}}fo" valued="{{$us["id"]}}" value="{{$us["estado"]}}" action="/ActivarUs/{{$us["id"]}}">
                                 {{ csrf_field()}}
                                 <input id="in{{$us["id"]}}" value="{{$us["estado"]}}" style="display:none;">
   								@if($us['estado']==1)
									<a id="{{$us["id"]}}-boton-v" href="#" class="btn btn-success btn-precionar"   data-toggle="tooltip" data-placement="top" title="Desactivar">
									<i class="material-icons">check_circle</i></a>
									<a style="display:none"id="{{$us["id"]}}-boton-r"href="#" class="btn btn-danger btn-precionar"  data-toggle="tooltip" data-placement="top" title="Activar">
									<i class="material-icons">highlight_off</i></a>
								@endif
								@if($us["estado"]==0)
								<a  style="display:none;"id="{{$us["id"]}}-boton-v" href="#" class="btn btn-success btn-precionar"   data-toggle="tooltip" data-placement="top" title="Desactivar">
									<i class="material-icons">check_circle</i></a>
									<a  id="{{$us["id"]}}-boton-r"href="#" class="btn btn-danger btn-precionar"  data-toggle="tooltip" data-placement="top" title="Activar">
									<i class="material-icons">highlight_off</i></a>
									
								@endif
 								</form>
 								</div>

 						 	</div>
					</div>
					</br>
				    </br>
					</div>

      				@endforeach
      	
						
       		 	</div>
       		    </br>
				</br>
    		</div>
  		</div>
 @endforeach
    </div>
</br></br>
</div>



    
  </div>
</div>

<script type="text/javascript">
  	$( document ).ready(function() {
   
    $('[data-toggle="tooltip"]').tooltip(); 

    $(".btn-precionar").click(function(e){
    	e.preventDefault();
    	
    		
    		
    		var form=$(this).parents('form');
    		var url=form.attr('action');
    		var valor2=form.attr('valued');
    		var valor=$('#in'+valor2).val();
    		

    		if(valor==1)
    		{

    			$("#"+valor2+"-boton-v").slideUp( 10 );

    			$("#"+valor2+"-boton-r").fadeIn( 1000 );
    			$("#in"+valor2).val("0");
    			
    			
    		}
    		else
    		{
    			
    			$("#"+valor2+"-boton-r").slideUp( 10 );
    			$("#"+valor2+"-boton-v").fadeIn( 1000 );
    			$("#in"+valor2).val("1");
    			
    		}
    		$.post(url,form.serialize(),function(result){
    			
    			});

    		
    		
    		
    });
   
    });
</script>


@endsection