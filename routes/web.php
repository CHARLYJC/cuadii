<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify'=>true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
Route::get('/Usuarios','UsuariosController@consul')->middleware('verified');

//Route::get('/ActivarUs/{id}','UsuariosController@activa')->middleware('verified');
Route::post('/ActivarUs/{id}','UsuariosController@activar');

Route::resource('/Datos_p/{id}','DatosPController');