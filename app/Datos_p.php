<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datos_p extends Model
{
     protected $table = 'datos_p';
    protected $primaryKey='id';
    protected $fillable=[
                    
                'nombre',
                'ap',
                'am',
                'genero',
                'edad',
                'telefono',
                'celular',
                'nacionalidad',
                'id_mun_dir',
                'localidad',
                'colonia',
                'calle',
                'no_i',
                'no_e',
                'otra_ref',
                'id_us',
                'imagen'];
}
