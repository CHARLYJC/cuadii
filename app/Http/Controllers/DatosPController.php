<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Datos_p;
use Illuminate\Support\Facades\Storage;

class DatosPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $mensage="";
        $datos=DB::selectOne('select*from datos_p where datos_p.id_us='.$id.'');
        if ($datos==null) 
        {
             $array=array(
                        'id_us'=>$id,
                        'imagen'=>"usr.png"
                        );

          Datos_p::create($array);
          $datos=DB::selectOne('select*from datos_p where datos_p.id_us='.$id.'');
       

        return view('Usuarios.Datos_Personales',compact("id",'datos','mensage'));

        }
        else
        {

        return view('Usuarios.Datos_Personales',compact("id",'datos','mensage'));
        }




    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $fdata=$_FILES["imagen"];
         $id=$request->get('id');
          Storage::disk('Avatar')->put("avatar".$id,file_get_contents($_FILES["imagen"]['tmp_name']));
        $id=$request->get('id');

  $datos=array(
                'nombre'=>$request->get('nombre'),
                'ap'=>$request->get('ap'),
                'am'=>$request->get('am'),
                'genero'=>$request->get('genero'),
                'edad'=>$request->get('edad'),
                'telefono'=>$request->get('telefono'),
                'celular'=>$request->get('celular'),
                'nacionalidad'=>$request->get('nacionalidad'),
                'id_mun_dir'=>$request->get('municipio'),
                'localidad'=>$request->get('localidad'),
                'colonia'=>$request->get('colonia'),
                'calle'=>$request->get('calle'),
                'no_i'=>$request->get('no_i'),
                'no_e'=>$request->get('no_e'),
                'otra_ref'=>$request->get('otra_ref'),
                'imagen' => "avatar".$id,
               
            );
 Datos_p::find($id)->update($datos);
  $mensage="Datos modificados correctamente";
   $datos=DB::selectOne('select*from datos_p where datos_p.id_us='.$id.'');

         return view('Usuarios.Datos_Personales',compact("id",'datos','mensage'));



    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
