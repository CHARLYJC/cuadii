<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $hola="usuarios";
       // return view('Usuarios.Usuarios',compact('hola'));
         return view('Usuarios.Usuarios',compact('hola'));
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

public function activar(Request $request, $id)
    {
        if($request->ajax())
        {

            $estado=User::find($id);

            $es=$estado->estado;

            if ($es==1)
             {
                $es=0;
            }
            else
            {
                $es=1;
            }

              $datos = array(
   
            'estado'=>$es);

            User::find($id)->update($datos);
           return response()->json([
           'mensage'=>$estado->name.'fue modificado correctamente'
           ]);
        }

    }
public function activa($id)
    {

            $estado=DB::selectOne("select*from users where users.id=".$id."");
            $estado=$estado->estado;

            if ($estado==1)
             {
                $estado=0;
            }
            else
            {
                $estado=1;
            }

              $datos = array(
   
            'estado'=>$estado);

            User::find($id)->update($datos);
            return redirect('/Usuarios');
    }

     public function consul()
    {
      

$tipos=DB::select('select*from tipo_us');


$datos_tipo=array();
foreach ($tipos as $t)
{
    $dat['id_tipo_us']=$t->id_tipo_us;
    $dat['Nombre']=$t->Nombre;
     
    $users=DB::select('select*from users where users.tipo='.$t->id_tipo_us.'');
    $datos_us=array();
    foreach ($users as $ca)
    {
        $us['id']=$ca->id;
        $us['name']=$ca->name;
        $us['email']=$ca->email;
        $us['estado']=$ca->estado;
       
        array_push($datos_us,$us);   
        //dd($carta_menu); 
    }
    
    $dat['us']=$datos_us;
    array_push($datos_tipo, $dat);
}
$mensage="";
 return view('Usuarios.Usuarios',compact('mensage'))->with(['usuarios'=>$datos_tipo]);

}
}
